import numpy
from scipy import stats
from scipy.fft import dct, idct, fft, ifft
from matplotlib import pyplot as plt
import statistics
import time
import os
import psutil

def compress_runlength(data):
    data_transform = dct(data, norm='ortho')# transforms data into the frequency domain

    n = len(data_transform)
    quarter = n//4

    sorted_transform = sorted(abs(data_transform), reverse=True)
    k_highest = sorted_transform[quarter]

    for i in range(n):
        if abs(data_transform[i]) <= k_highest:
            data_transform[i] = 0

    return data_transform

def compress_tuples(data):
    data_transform = dct(data, norm='ortho')  # transforms data into the frequency domain

    n = len(data_transform)
    quarter = n // 4



    indexed = [0] * n  # create empty array to store tuples
    for i in range(n):
        indexed[i] = [i, data_transform[i]]  # create tuple with coefficient and original position of coefficient

    indexed = sorted(indexed, key=lambda x: abs(x[1]), reverse=True)  # sort list by magnitude of fourier coefficient
    indexed = indexed[:quarter]

    return indexed, n





def encode(data):
    length = len(data)
    encoded = []
    i = 0
    while i < length:
        if (data[i]==0):
            count = 0
            while data[i] == 0:
                count = count+1
                i = i+1
                if (i == length):
                    break
            encoded.append(int(count))
        else:
            encoded.append(data[i])
            i = i+1
    return encoded

def decode(data):
    output = []
    i = 0
    while i < len(data):

        if isinstance(data[i],int)==True:
            counter = 0
            while(counter < data[i]):
                output.append(0)
                counter = counter+1
            i = i+1
        else:
            output.append(data[i])
            i = i+1

    return output


def compress(data):
    data_transform = dct(data, norm = 'ortho') #transforms data into the frequency domain
    n = len(data_transform)

    right_index = n//4#obtain first 25  percent of the dct coefficients
    data_transform = data_transform[:right_index]

    return data_transform, n

def decompress_runlength(data):
    out_time = idct(data, norm='ortho')
    return out_time

def decompress_tuples(data, size):
    reformatted = [0] * size

    for i in range(len(data)):
        index = data[i][0]
        reformatted[index] = data[i][1]

    out_time = idct(reformatted, norm='ortho')
    return out_time


def decompress(data, size):
    right = size//4
    out = numpy.zeros(size-right)
    out = numpy.append(data,out)
    out_time = idct(out, norm='ortho')
    return out_time

def ReadFile(filename):
    f = open(filename,"r")
    data_string_array = f.read().splitlines()
    data_string_array = numpy.asarray(data_string_array)
    floats = data_string_array.astype(numpy.float)
    return floats

def WriteFile(filename, data):
    f = open(filename, "w")
    strings = []
    for element in data:
        strings.append(str(element)+"\n")
    print(strings)
    f.writelines(strings)

#tests runlength compression
def test_runlength(raw_data):
    time_start = time.time()

    raw = numpy.asarray(raw_data)
    input_bytes = raw.nbytes
    print(f"Input takes up {input_bytes} bytes")

    compressed_data_2 = compress_runlength(raw_data)

    encoded_data = encode(compressed_data_2)

    encoded_test = numpy.asarray(encoded_data)
    output_bytes = encoded_test.nbytes
    print(f"Output takes up {output_bytes} bytes")

    decoded_data = decode(encoded_data)
    decompressed_data = decompress_runlength(decoded_data)

    MSE = numpy.square(numpy.subtract(raw_data, decompressed_data)).mean()
    print(f"Mean Squared Error: {MSE}")

    time_end = time.time()
    elapsed = time_end-time_start
    print(f"Elapsed Time: {elapsed}s")


#test low pass filter compression
def test_LPF(raw_data):
    time_start = time.time()

    raw = numpy.asarray(raw_data)
    input_bytes = raw.nbytes
    print(f"Input takes up {input_bytes} bytes")

    compressed_data, size = compress(raw_data)

    output_bytes = compressed_data.nbytes
    print(f"Output takes up {output_bytes} bytes")

    decompressed_data = decompress(compressed_data, size)

    MSE = numpy.square(numpy.subtract(raw_data, decompressed_data)).mean()
    print(f"Mean Squared Error: {MSE}")

    time_end = time.time()
    elapsed = time_end-time_start
    print(f"Elapsed Time: {elapsed}s")


def test_tuples(raw_data):
    time_start = time.time()

    raw = numpy.asarray(raw_data)
    input_bytes = raw.nbytes
    print(f"Input takes up {input_bytes} bytes")

    compressed_data, size = compress_tuples(raw_data)
    output_test = numpy.asarray(compressed_data)
    output_bytes = output_test.nbytes

    print(f"Output takes up {output_bytes} bytes")

    decompressed_data = decompress_tuples(compressed_data, size)
    decompressed_test = numpy.asarray(decompressed_data)
    MSE = numpy.square(numpy.subtract(raw_data, decompressed_test)).mean()
    print(f"Mean Squared Error: {MSE}")

    time_end = time.time()
    elapsed = time_end-time_start
    print(f"Elapsed Time: {elapsed}s")



def testplot(raw_data, decompressed_data):
    x = numpy.arange(0, raw_data.size)

    # Set up a subplot grid that has height 2 and width 1,
    # and set the first such subplot as active.
    plt.subplot(2, 1, 1)

    # Make the first plot
    plt.plot(x, raw_data)
    plt.title('before compression')

    # Set the second subplot as active, and make the second plot.
    plt.subplot(2, 1, 2)
    plt.plot(x, decompressed_data)
    plt.title('after decompression')

    # Show the figure.
    plt.show()


def main():
    raw_data = ReadFile("data_ACCNED1.txt")

    pid = os.getpid()
    python_process = psutil.Process(pid)

    cpuUse = python_process.cpu_percent(interval=None)/psutil.cpu_count()
    test_runlength(raw_data)
    cpuUse = python_process.cpu_percent(interval=None)/psutil.cpu_count()

    memoryUse = (python_process.memory_info()[0]/(1*(10**6)))  # memory usage in kilobytes

    print('memory use:', memoryUse)
    print('CPU use:', cpuUse)


if __name__ == '__main__':
    main()



















