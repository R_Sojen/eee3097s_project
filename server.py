
import socket
import threading
import compress
import pickle
import IMU

HEADER = 64 #specify size of header
PORT = 8000 #specify socket port number
SERVER = "192.168.101.218"
FORMAT = "utf-8"
DISCONNECT_MESSAGE = "!DISCONNECT" #specify message that will be sent from the client when the server needs to disconncect
Data_Types = ["pitch", "roll", "yaw", "acceleration_x", "acceleration_y", "acceleration_z", "gyro_x", "gyro_y", "gyro_z", "mag_x", "mag_y", "mag_z"]

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)#creates a socket with ipv4 and tcp protocol
s.bind((SERVER,PORT))#binds socket to port


def handle_client(conn, addr):
    print(f"NEW CONNECTION FROM {addr}")
    connected = True
    while connected:
        msg_length = conn.recv(HEADER).decode(FORMAT) #header message telling server the size of the next message
        
        if msg_length:

            msg_length = int(msg_length)
            msg = conn.recv(msg_length) #data message


            if msg == DISCONNECT_MESSAGE:
                connected = False
                continue


            raw_data = IMU.sample(10000)
        

            for i in range(12):
                msg_length = conn.recv(HEADER).decode(FORMAT)
                msg_length = int(msg_length)
                msg = conn.recv(msg_length).decode(FORMAT)
                
                
                #print(Data_Types[i])
                #print(raw_data[i])

                compressed_data = compress.encode(compress.compress_runlength(raw_data[i]))
                pickled_data = pickle.dumps(compressed_data)
                msg_length_pickled = len(pickled_data)
                send_length = bytes(f'{msg_length_pickled:<{HEADER}}',FORMAT) #sends formatting message length value, left aligned by the header size
                conn.send(send_length)
                conn.send(pickled_data)

            

            
    conn.close()



#starts the server and implements multithreading to allow for multiple clients to connect
def start():
    s.listen()
    print(f"SERVER IS LISTENING ON {SERVER}...")
    while True:
        conn, addr = s.accept()
        print(addr)
        print(conn)
        thread = threading.Thread(target = handle_client, args = (conn, addr))
        thread.start()
        print(f"[ACTIVE CONNECTIONS] {threading.activeCount()-1}")


    

print("SERVER IS STARTING...")
start()


